FROM ruby:2.5.1
RUN apt-get update &&\
    apt-get -y install \
                       libpq-dev \
                       build-essential \
                       libxml2-dev \
                       libcurl4-openssl-dev \
                       libssl-dev \
                       zlib1g-dev \
                       libzip-dev \
                       zip \
                       default-libmysqlclient-dev \
                       openssl \
                       libffi-dev \
                       ncurses-dev \
                       curl &&\
                       apt-get clean autoclean &&\
    apt-get autoremove --yes &&\
    rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN useradd -m testuser
RUN chmod -R 700 /sbin && \
    chmod -R 733 /tmp &&\
    chmod +t /tmp && \
    chmod -R 700 /home/testuser
    #chmod -R 700 /bin &&\
    #chmod -R 700 /usr/bin &&\
    #chmod -R 700 /usr/local/bin &&\
    #chmod -R 700 /bin && \

COPY example /app/example
USER testuser
WORKDIR /app/example

RUN bundle install -j $(nproc)

CMD bundle exec ruby spec/acceptance_test.rb

