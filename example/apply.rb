# frozen_string_literal: true
require 'puppet'
require 'tmpdir'


class PuppetApply
  ROOT_DIR = File.dirname(__FILE__)
  MODULES_PATH = File.join(ROOT_DIR, 'modules')
  REPO_DIR = ROOT_DIR
  MANIFESTS_DIR = File.join(REPO_DIR, 'maniests')
  HIERA_CONFIG = File.join(REPO_DIR, 'hiera.yaml')
  SITE_PP = File.join(MANIFESTS_DIR, 'site.pp')
  attr_accessor :options

  def initialize(opts = {})
    @options = opts
  end

  def self.puppet_setup
    begin
      Puppet.initialize_settings
    rescue Puppet::DevError

    end
    #Puppet.initialize_facts
    Puppet.settings[:modulepath] = Array(MODULES_PATH)
    Puppet.settings[:basemodulepath] = Array(MODULES_PATH)
    Puppet.settings[:confdir] = REPO_DIR
    Puppet.settings[:hiera_config] = HIERA_CONFIG
    # Puppet.settings[:user]  = '0'
    # Puppet.settings[:group] = '0'
    # the environment must be set, otherwise the catalog does not get applied
    Puppet.settings[:environment] = '*root*'
  end

  def self.generate_catalog(opts = {})
    catalog = nil
    time = nil
    Dir.mktmpdir do |dir|
      Puppet[:code] = opts[:code]
      Puppet.settings[:confdir] = File.join(dir, 'conf')
      Puppet.settings[:vardir] = File.join(dir, 'var')
      Puppet.settings[:logdir] = File.join(dir, 'log')
      Puppet.settings[:codedir] = File.join(dir, 'code')
      Puppet.settings[:rundir] = File.join(dir, 'run')
      Puppet.settings[:ssldir] = File.join(dir, 'ssl')

      puppet_setup
      Puppet[:manifest] = SITE_PP unless opts[:code]
      p = PuppetApply.new
      defaults = app_defaults
      p.options = defaults.merge(opts)
      catalog, time = p.generate_catalog
    end
    [catalog, time]
  end

  def self.app_defaults
    { test: true,
      modulepath: Array(MODULES_PATH), hiera_config: HIERA_CONFIG,
      default_file_terminus: :file_server, write_catalog_summary: true }
  end

  def generate_catalog(facts = {})
    setup
    if Puppet[:code].empty?
      manifest = Puppet[:manifest]
      unless Puppet::FileSystem.exist?(manifest)
        raise format(_('Could not find file %{manifest}'),
                     manifest: manifest)
      end
    end

    unless Puppet[:node_name_fact].empty?
      # Collect our facts.
      unless facts = Puppet::Node::Facts.indirection.find(Puppet[:node_name_value])
        raise format(_('Could not find facts for %{node}'), node: Puppet[:node_name_value])
      end

      Puppet[:node_name_value] = facts.values[Puppet[:node_name_fact]]
      facts.name = Puppet[:node_name_value]
    end

    # Find our Node
    unless node = Puppet::Node.indirection.find(Puppet[:node_name_value])
      raise format(_('Could not find node %{node}'), node: Puppet[:node_name_value])
    end

    configured_environment = node.environment || Puppet.lookup(:current_environment)
    configured_environment = configured_environment.override_with(hiera_config: HIERA_CONFIG,
                                                                  modulepath: Array(options[:modulepath]))
    apply_environment = manifest ? configured_environment.override_with(manifest: manifest) : configured_environment

    # Modify the node descriptor to use the special apply_environment.
    # It is based on the actual environment from the node, or the locally
    # configured environment if the node does not specify one.
    # If a manifest file is passed on the command line, it overrides
    # the :manifest setting of the apply_environment.
    node.environment = apply_environment

    # TRANSLATORS "puppet apply" is a program command and should not be translated
    Puppet.override({ current_environment: apply_environment }, _('For puppet apply')) do
      # Merge in the facts.
      node.merge(facts.values) if facts

      # Add server facts so $server_facts[environment] exists when doing a puppet apply
      node.add_server_facts({})

      # Allow users to load the classes that puppet agent creates.
      if options[:loadclasses]
        file = Puppet[:classfile]
        if Puppet::FileSystem.exist?(file)
          unless FileTest.readable?(file)
            warn format(_('%{file} is not readable'), file: file)
            exit(63)
          end
          node.classes = Puppet::FileSystem.read(file, encoding: 'utf-8').split(/[\s]+/)
        end
      end
      begin
        # Compile the catalog
        starttime = Time.now

        # When compiling, the compiler traps and logs certain errors
        # Those that do not lead to an immediate exit are caught by the general
        # rule and gets logged.
        #
        catalog = Puppet::Resource::Catalog.indirection.find(node.name, use_node: node)
        elapsed = Time.now.to_i - starttime.to_i
        return [catalog, elapsed ]
      end
    end
  end


  def main
    begin
      catalog, seconds = generate_catalog
      # Translate it to a RAL catalog
      catalog = catalog.to_ral

      catalog.finalize

      catalog.retrieval_duration = Time.now - starttime

      if options[:write_catalog_summary]
        catalog.write_class_file
        catalog.write_resource_file
      end
      exit_status = Puppet.override(loaders: Puppet::Pops::Loaders.new(apply_environment)) { apply_catalog(catalog) }

      if !exit_status
        return 1
      elsif options[:detailed_exitcodes]
        return exit_status
      else
        return 0
      end
    end
  end

# Enable all of the most common test options.
  def setup_test
    Puppet.settings.handlearg('--no-splay')
    Puppet.settings.handlearg('--show_diff')
    options[:verbose] = true
    options[:detailed_exitcodes] = true
  end

  def set_log_level(opts = nil)
    opts ||= options
    if opts[:debug]
      Puppet::Util::Log.level = :debug
    elsif opts[:verbose] && !Puppet::Util::Log.sendlevel?(:info)
      Puppet::Util::Log.level = :info
    end
  end

  def setup
    # @tmp_log = Tempfile.new('puppeteer_apply')
    # pp.handle_logdest_arg(@tmp_log.path)

    setup_test if options[:test]
    Puppet.settings.print_configs

    Puppet::Util::Log.newdestination(:syslog) unless options[:setdest]

    Signal.trap(:INT) do
      warn _('Exiting')
      return 1
    end

    Puppet.settings.use :user, :main, :agent, :ssl

    Puppet::Resource::Catalog.indirection.cache_class = Puppet[:catalog_cache_terminus] if Puppet[:catalog_cache_terminus]

    # we want the last report to be persisted locally
    Puppet::Transaction::Report.indirection.cache_class = :yaml

    set_log_level
  end

end
