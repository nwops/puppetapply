require 'tmpdir'
require 'json'

ROOT_DIR = File.dirname(__FILE__)
MODULES_PATH = File.join(ROOT_DIR, 'modules')
REPO_DIR = ROOT_DIR
MANIFESTS_DIR = File.join(REPO_DIR, 'maniests')
HIERA_CONFIG = File.join(REPO_DIR, 'hiera.yaml')
SITE_PP = File.join(MANIFESTS_DIR, 'site.pp')
TYPES_GLOB = File.join(MODULES_PATH, '*', 'lib', 'puppet', 'type')
PROVIDERS_GLOB = File.join(MODULES_PATH, '*', 'lib', 'puppet', 'provider')

# @return [Object] - The serialized code in json format
# This method is called by the serverless container when requests are made
def handler(event)
  body = event.body.kind_of?(String) ? JSON.parse(event.body) : event.body
  data = body.merge(event.query_params)
  opts = {
      code: code(data['code']),
      facts: parse_facts(data['facts'])
  }
  obj = pal(opts)
  obj.to_json
end

# @return [String] - the puppet code the starts everything
def code(code_string)
  return code_string unless code_string.nil? || code_string.empty?
  # It is assumed you will have a production module with the site.pp file in it
  site_pp = File.join(MODULES_PATH, 'production', 'manifests', 'site.pp')
  File.exist?(site_pp) ? File.read(site_pp) : ""
end

# @return [Hash] - a hash of just the facts
# @param data [Hash] - a hash of facts in the { values: facts_hash } format
# this value is returned from puppet facts command
def parse_facts(data = {})
  return {} if data.nil?
  data.fetch("values", data)
end

# @return [Array] - a list of type files
# @param type_filter [Array] - a list of types or glob pattern to filter all the types with
def types_by_filter(type_filter = ['*'])
  type_filter.map { |type| Dir.glob(File.join(TYPES_GLOB, "#{type}.rb")) }.flatten
end

# @return [Array] -  a list of provider files that will be used by the given type filter
def providers_by_type(type_filter = ['*'])
  type_filter.map { |type| Dir.glob(File.join(PROVIDERS_GLOB, "#{type}", '**', '*.rb')) }.flatten
end

# @return [Array] - a list of provider files found with the filter, returns all providers by default
def providers_by_filter(type_filter = ['*'], provider_filter = nil)
  # This does not get libraries outside of the provider directory
  type_providers = providers_by_type(type_filter)
  return type_providers if provider_filter.nil? or provider_filter.empty?
  provider_filter.find_all { |p| type_providers.include?(p) }
end

# @return [Array] - returns a list of type/provider files that are utilized in the catalog
# @note this does not include 3rd party gems or external libraries and load paths
def plugin_files(catalog)
  types = catalog['resources'].map {|d| d['type'].downcase }.uniq
  type_files = types_by_filter(types)
  provider_files = providers_by_filter(types)
  files = type_files + provider_files
  files.map do |file|
    file.split(MODULES_PATH).last
  end
end

# @return [Hash] - returns a hash of plugin files as content
# {filename => content}
def plugin_data(catalog)
  data = {}
  plugin_files(catalog).each do |file|
    data[file] = File.read(File.join(MODULES_PATH, file))
  end
  data
end

# @return [Hash] - returns a hash of the catalog and plugin data
# @param code [String] - the puppet code that should be compiled
# @param facts [Hash]  - a hash of fact data from the client
def pal(opts = {})
  require 'puppet_pal'
  Dir.mktmpdir do |dir|
    begin
      Puppet.settings[:confdir] = File.join(dir, 'conf')
      Puppet.settings[:vardir] = File.join(dir, 'var')
      Puppet.settings[:logdir] = File.join(dir, 'log')
      Puppet.settings[:codedir] = File.join(dir, 'code')
      Puppet.settings[:rundir] = File.join(dir, 'run')
      #Puppet.settings[:loglevel] = :none
      Puppet.settings[:hiera_config] = HIERA_CONFIG
      Puppet.initialize_settings
    rescue Puppet::DevError
      # do nothing
    end
    settings = {}

    Puppet[:rich_data] = true
    catalog = Puppet::Pal.in_tmp_environment('production',
                                             modulepath: [MODULES_PATH],
                                             settings_hash: settings, facts: opts[:facts], variables: {}) do |pal|

      pal.with_catalog_compiler do |c|
        c.evaluate_string(opts[:code])
        #c.compiler_additions # eval lazy constructs and validate again
        c.with_json_encoding { |encoder| encoder.encode }
      end
    end
    # we have to parse here because the faastruby returns to_json for us so it expects a object that can be
    # serialized already
    parsed_catalog = JSON.parse(catalog)
    return {
        catalog: parsed_catalog,
        plugins: plugin_data(parsed_catalog)
    }

  end

end
