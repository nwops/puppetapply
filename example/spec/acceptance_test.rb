require_relative './spec_helper'
require 'json'
require_relative '../handler'

event = HttpEvent.new(body: File.read(File.join(fixtures_dir, 'body.json')))

resp = handler(event)
puts JSON.pretty_generate(JSON.parse(resp))
